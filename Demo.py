import numpy as np
import matplotlib.pyplot as plt
load = np.loadtxt('ex2data1.txt', delimiter=',', usecols=range(3))
g1x = []
g1y = []
g2x = []
g2y = []
for i in range(100):
    if(load[i,2]==0):
        g1x.append(load[i,0])
        g1y.append(load[i,1])
    else:
        g2x.append(load[i,0])
        g2y.append(load[i,1])
g1 = (np.array(g1x),np.array(g1y))
g2 = (np.array(g2x),np.array(g2y))
data = (g1,g2)

colors = ("yallow", "black")
groups = ("Admitted", "Not Admitted")
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

for data, color, group in zip(data, colors, groups):
    x,y = data
ax.scatter(x, y, alpha=0.8, c=color, edgecolors='none', s=30, label=group)

plt.title('Matplot scatter plot')
plt.legend(loc=2)
plt.show()

