import cv2 as cv
import numpy as np
import tensorflow as tf
from mtcnn.mtcnn import MTCNN

tail = '.JPG'
wd = 'img'
folder = 'img/g/0'

for i in range(1,9):
    path = folder+str(i)+tail
    src = cv.imread(path)

    detector = MTCNN()
    result = detector.detect_faces(src)
    k = 0

    for person in result:
        bounding_box = person['box']
        print(bounding_box)
        y_min = bounding_box[0]
        x_min = bounding_box[1]
        y_max = bounding_box[0] + bounding_box[2]
        x_max = bounding_box[1] + bounding_box[3]
        if(x_min < 0):
            x_min = 0
        if(y_min < 0):
            y_min = 0

        sub_img = src[x_min:x_max,y_min:y_max,:]
        cv.imwrite('face/' + str(i)+'_'+str(k)+'.png', sub_img)
        k += 1


