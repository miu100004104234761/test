import cv2 as cv
import numpy as np
import tensorflow as tf
from mtcnn.mtcnn import MTCNN
cap = cv.VideoCapture(0)
detector = MTCNN()
while True:
    # Capture frame-by-frame
    __, frame = cap.read()

    # Use MTCNN to detect facesz
    result = detector.detect_faces(frame)
    if result != []:
        for person in result:
            bounding_box = person['box']
            keypoints = person['keypoints']

            cv.rectangle(frame,
                          (bounding_box[0], bounding_box[1]),
                          (bounding_box[0] + bounding_box[2], bounding_box[1] + bounding_box[3]),
                          (0, 155, 255),
                          2)

            cv.circle(frame, (keypoints['left_eye']), 2, (0, 155, 255), 2)
            cv.circle(frame, (keypoints['right_eye']), 2, (0, 155, 255), 2)
            cv.circle(frame, (keypoints['nose']), 2, (0, 155, 255), 2)
            cv.circle(frame, (keypoints['mouth_left']), 2, (0, 155, 255), 2)
            cv.circle(frame, (keypoints['mouth_right']), 2, (0, 155, 255), 2)
    # display resulting frame
    cv.imshow('frame', frame)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break
# When everything's done, release capture
cap.release()
cv.destroyAllWindows()